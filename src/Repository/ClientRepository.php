<?php

namespace App\Repository;

use App\Entity\Client;
use App\Lib\Enumeration\Ulogin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Client::class);
    }

    public function findOneByPassportAndEmail(string $passport, string $email) : ?Client
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.passport = :passport')
                ->orWhere('a.email = :email')
                ->setParameter('passport', $passport)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByPasswordAndEmail(string $password, string $email) : ?Client
    {

        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.password = :password')
                ->andWhere('a.email = :email')
                ->setParameter('password', $password)
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByEmail(string $email) : ?Client
    {
        try {
            return $this->createQueryBuilder('c')
                ->select('c')
                ->andWhere('c.email = :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function findOneByUid(string $uid, string $network) : ?Client
    {
        try {

            $result = $this->createQueryBuilder('c')
                ->select('c')
            ;

            if ($network == Ulogin::FACEBOOK) {
                $result->andWhere("c.faceBookId = :uid");
            } else if ($network == Ulogin::VKONTAKTE) {
                $result->andWhere("c.vkId = :uid");
            } else if ($network == Ulogin::GOOGLE) {
                $result->andWhere("c.googleId = :uid");
            }

            return $result
                ->setParameter("uid", $uid)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();

        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
