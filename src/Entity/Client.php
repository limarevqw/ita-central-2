<?php

namespace App\Entity;

use App\Lib\Enumeration\Ulogin;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClientRepository")
 */
class Client extends AbstractBaseEntity
{

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=254, unique=true, nullable=true)
     */
    private $passport;

    /**
     * @ORM\Column(type="string", length=254, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=254, unique=true, nullable=true)
     */
    private $vkId;

    /**
     * @ORM\Column(type="string", length=254, unique=true, nullable=true)
     */
    private $faceBookId;

    /**
     * @ORM\Column(type="string", length=254, unique=true, nullable=true)
     */
    private $googleId;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getRegisteredAt(): ?\DateTime
    {
        return $this->registeredAt;
    }

    /**
     * @param \DateTime $registeredAt
     */
    public function setRegisteredAt(\DateTime $registeredAt): void
    {
        $this->registeredAt = $registeredAt;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     * @return Client
     */
    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @param mixed $email
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $passport
     * @return Client
     */
    public function setPassport($passport)
    {
        $this->passport = $passport;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPassport()
    {
        return $this->passport;
    }

    /**
     * @param mixed $vkId
     * @return Client
     */
    public function setVkId($vkId)
    {
        $this->vkId = $vkId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVkId()
    {
        return $this->vkId;
    }

    /**
     * @param mixed $faceBookId
     * @return Client
     */
    public function setFaceBookId($faceBookId)
    {
        $this->faceBookId = $faceBookId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFaceBookId()
    {
        return $this->faceBookId;
    }

    /**
     * @param mixed $googleId
     * @return Client
     */
    public function setGoogleId($googleId)
    {
        $this->googleId = $googleId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGoogleId()
    {
        return $this->googleId;
    }

    /**
     * @param $network
     * @param $uid
     * @return Client
     */
    public function setNetworkUlogin($network, $uid)
    {
        if ($network == Ulogin::GOOGLE) {
            $this->setGoogleId($uid);
        } else if ($network == Ulogin::FACEBOOK) {
            $this->setFaceBookId($uid);
        } else if ($network == Ulogin::VKONTAKTE) {
            $this->setVkId($uid);
        }
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            'password' => $this->getPassword(),
            'email' => $this->getEmail(),
            'passport' => $this->getPassport(),
            'vkId' => $this->getVkId(),
            'faceBookId' => $this->getFaceBookId(),
            'googleId' => $this->getGoogleId()
        ];
    }



}
