<?php

namespace App\Controller;

use App\Model\Client\ClientHandler;
use App\Repository\ClientRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/ping", name="app_index")
     */
    public function pingAction()
    {
        return new JsonResponse(['result' => 'pong']);
    }

    /**
     * @Route("/client-encode-password/{password}", name="/client_encode_password")
     * @param ClientHandler $clientHandler
     * @param string $password
     * @return JsonResponse
     */
    public function encodePasswordAction(ClientHandler $clientHandler ,string $password)
    {
        return new JsonResponse([
            'result' => $clientHandler->encodePassword($password)
        ]);
    }

    /**
     * @Route("/client/{passport}/{email}", name="app_client_exists")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAction(
        string $passport,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPassportAndEmail($passport, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/auth-client-exists/{password}/{email}", name="auth_client_exists")
     * @Method("HEAD")
     * @param string $password
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function authClientExistsAction(
        string $password,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPasswordAndEmail($password, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/get-one-data-client/{email}", name="app_get_one_data_client")
     * @Method("GET")
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function getOneDataClientAction(
        string $email,
        ClientRepository $clientRepository)
    {
        $result = $clientRepository->findOneByEmail($email);

        if ($result) {
            $data = $result->toArray();
            $status = true;
        }
        return new JsonResponse([
            'data' => isset($data) ? $data : null ,
            'status' => isset($status) ?? false
        ]);
    }

    /**
     * @Route("/client", name="app_create_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');

        if(empty($data['email']) || empty($data['passport']) || empty($data['password'])) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: '.var_export($data,1)],406);
        }

        if ($clientRepository->findOneByPassportAndEmail($data['passport'], $data['email'])) {
            return new JsonResponse(['error' => 'Клиент уже существует'],406);
        }

        $client = $clientHandler->createNewClient($data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/ulogin-client-exists/{uid}/{network}", name="ulogin_client_exists")
     *
     * @Method("HEAD")
     * @param string $uid
     * @param string $network
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     * @internal param string $password
     */
    public function uloginClientExistsClientAction(
        string $uid,
        string $network,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByUid($uid, $network)) {
            return new JsonResponse(['status' => true]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/ulogin-data-client/{uid}/{network}", name="ulogin_data_client")
     *
     * @Method("GET")
     * @param string $uid
     * @param string $network
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     * @internal param string $password
     */
    public function uloginDataClientAction(
        string $uid,
        string $network,
        ClientRepository $clientRepository)
    {
        $result = $clientRepository->findOneByUid($uid, $network);

        if ($result) {
            return new JsonResponse(['result' => $result->toArray()]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/create-client-soc", name="app_create_soc_client")
     * @Method("POST")
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createSocClientAction(
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->get('email');
        $data['uid'] = $request->get('uid');
        $data['passport'] = $request->get('passport');
        $data['password'] = $request->get('password');
        $network = $request->get('network');

        $client = $clientHandler->createNewClient($data, $network);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/update-client-soc", name="app_update_soc_client")
     * @Method("PUT")
     * @param ObjectManager $manager
     * @param Request $request
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @return JsonResponse
     */
    public function updateSocClientAction(
        ObjectManager $manager,
        Request $request,
        ClientRepository $clientRepository,
        ClientHandler $clientHandler
    )
    {
        $data['email'] = $request->get('email');
        $data['passport'] = $request->get('passport');
        $data['faceBookId'] = $request->get('faceBookId');
        $data['googleId'] = $request->get('googleId');
        $data['vkId'] = $request->get('vkId');
        $data['password'] = $request->get('password');

        $client = $clientHandler->updateClient($clientRepository, $data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }
}
