<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 9:53 PM
 */

namespace App\Model\Client;

use App\Entity\Client;
use App\Repository\ClientRepository;

class ClientHandler
{
    /**
     * @param array $data
     * @param bool $network
     * @return Client
     */
    public function createNewClient(array $data, $network = false)
    {
        $client = new Client();
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);

        if ($network) {
            $client->setNetworkUlogin($network, $data['uid']);
        }

        $password = $this->encodePassword($data['password']);
        $client->setPassword($password);

        return $client;
    }

    /**
     * @param ClientRepository $clientRepository
     * @param array $data
     * @return Client
     */
    public function updateClient(ClientRepository $clientRepository, $data)
    {
        $client = $clientRepository->findOneByEmail($data['email']);

        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $client->setFaceBookId($data['faceBookId'] ?? null);
        $client->setGoogleId($data['googleId'] ?? null);
        $client->setVkId($data['vkId'] ?? null);
        $client->setPassword($data['password']);

        return $client;
    }

    /**
     * @param $password
     * @return string
     */
    public function encodePassword($password)
    {
        return md5($password).md5($password.'2');
    }
}
